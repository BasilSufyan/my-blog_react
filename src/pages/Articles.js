import React from 'react';
import ArticlesList from '../components/ArticlesList';
import articleContent from './article-content';
import NotfoundPage from './NotfoundPage';

function Articles({match}) {
    const name = match.params.name;    
    const article = articleContent.find(article =>  article.name ===name);
    const otherarticles = articleContent.filter( article => {if(article.name!==name) return article});

    if(!article){
        return <NotfoundPage></NotfoundPage>;
    }
    console.log(otherarticles);
    return (
        <>
           <h3>{article.title}</h3>
           <p>{article.content}</p>
           <br/>
           <h3>Other Articles:</h3>
           <ArticlesList articles={otherarticles} />

        </>
    )
}

export default Articles;

import React from 'react';

const NotfoundPage = () => (
    <h1>404: Page not found</h1>
)

export default NotfoundPage;
import './App.css';
import HomePage from './pages/HomePage';
import About from './pages/About';
import Articles from './pages/Articles';
import Navbar from './Navbar';
import {BrowserRouter as Router,Route,Switch}from 'react-router-dom';
import ArticlesListPage from './pages/ArticlesListPage';
import NotfoundPage from './pages/NotfoundPage';

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar/>
        <div id="page-body">
          <Switch>
            <Route path="/" component={HomePage} exact></Route>
            <Route path="/about" component={About} ></Route>
            <Route path="/articles-list" component={ArticlesListPage} ></Route>
            <Route path="/articles/:name" component={Articles} ></Route>
            <Route component={NotfoundPage} ></Route>
          </Switch>
        </div>        
      </div>
    </Router >

  );
}

export default App;
